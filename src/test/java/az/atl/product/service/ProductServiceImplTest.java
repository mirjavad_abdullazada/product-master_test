package az.atl.product.service;

import az.atl.product.dao.entity.ProductEntity;
import az.atl.product.dao.repository.ProductRepository;
import az.atl.product.model.dto.ProductDto;
import az.atl.product.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import az.atl.product.exception.ProductNotFoundException;


import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ProductServiceImplTest {
    @Mock
    private ProductRepository productRepository;
    private ProductServiceImpl productService;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        productService = new ProductServiceImpl(productRepository);
    }

    @Test
    void testGetAllProduct() {
        //Given
        List<ProductEntity> productEntities = List.of(
                new ProductEntity(1l, "Product 1", BigDecimal.valueOf(10.0), 5, "+994501234567"),
                new ProductEntity(2l, "Product 2", BigDecimal.valueOf(20.0), 10, "+994501234567")
        );

        when(productRepository.findAll()).thenReturn(productEntities);

        //When
        List<ProductDto> result = productService.getAllProduct();

        //Then
        assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertEquals("Product 1", result.get(0).getName()),
                () -> assertEquals("Product 2", result.get(1).getName())
        );
    }

        @Test
        void testGetProductById_Found() {
            // Given
            ProductEntity productEntity = new ProductEntity(1L, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012");
            when(productRepository.findById(1L)).thenReturn(java.util.Optional.of(productEntity));

            // When
            ProductDto result = productService.getProductById(1L);

            // Then
            assertAll(
                    () -> assertEquals("Product 1", result.getName()),
                    () -> assertEquals(BigDecimal.valueOf(10.0), result.getPrice()),
                    () -> assertEquals(5, result.getCount()),
                    () -> assertEquals("0123456789012", result.getPhoneNumber())
            );
    }
    @Test
    void testGetProductById_NotFound() {
        // Arrange
        when(productRepository.findById(1L)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(ProductNotFoundException.class, () -> productService.getProductById(1L));
    }
}
