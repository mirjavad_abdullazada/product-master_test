package az.atl.product.mapper;
import az.atl.product.dao.entity.ProductEntity;
import az.atl.product.model.dto.ProductDto;
import az.atl.product.model.mapper.ProductMapper;
import org.junit.jupiter.api.Test;


import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
public class ProductMapperTest {



    @Test
    void testBuildDtoList() {
        // Given
        List<ProductEntity> productEntities = List.of(
                new ProductEntity(1L, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012"),
                new ProductEntity(2L, "Product 2", BigDecimal.valueOf(20.0), 10, "0123456789012")
        );

        // When
        List<ProductDto> result = ProductMapper.buildDtoList(productEntities);

        // Then
        assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertEquals("Product 1", result.get(0).getName()),
                () -> assertEquals("Product 2", result.get(1).getName())
        );
    }

    @Test
    void testBuildDto() {
        // Given
        ProductEntity productEntity = new ProductEntity(1L, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012");

        // When
        ProductDto result = ProductMapper.buildDto(productEntity);

        // Then
        assertAll(
                () -> assertEquals(1L, result.getId()),
                () -> assertEquals("Product 1", result.getName()),
                () -> assertEquals(BigDecimal.valueOf(10.0), result.getPrice()),
                () -> assertEquals(5, result.getCount()),
                () -> assertEquals("0123456789012", result.getPhoneNumber())
        );


    }
}
