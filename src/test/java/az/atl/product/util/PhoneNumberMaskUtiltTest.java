package az.atl.product.util;

import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class PhoneNumberMaskUtiltTest {


    @Test
    void testMaskNumber_TrueCase() {
        var phoneNumber = "+994775050399";
        var excepted = "+994775****99";

        var maskedPhoneNumber = PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber);
        assertEquals(excepted, maskedPhoneNumber);
    }

    @Test
    void testMaskPhoneNumber_NullInput() {
        assertThrows(IllegalArgumentException.class,
                () -> PhoneNumberMaskUtil.maskPhoneNumber(null));
    }

    @Test
    void testMaskPhoneNumber_InvalidLengthInput() {
        String phoneNumber = "012345";
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber));
    }
}
