package az.atl.product.controller;

import az.atl.product.model.dto.ProductDto;
import az.atl.product.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductControllerTest {

    @Mock
    private ProductService productService;

    private ProductController productController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        productController = new ProductController(productService);
    }

    @Test
    void testGetAllProducts() {
        // Arrange
        List<ProductDto> productDtos = List.of(
                new ProductDto(1L, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012"),
                new ProductDto(2L, "Product 2", BigDecimal.valueOf(20.0), 10, "0123456789012")
        );
        when(productService.getAllProduct()).thenReturn(productDtos);

        // When
        List<ProductDto> result = productController.getAllProducts();

        // Then
        assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertEquals("Product 1", result.get(0).getName()),
                () -> assertEquals("Product 2", result.get(1).getName())
        );
    }

    @Test
    void testGetProduct_ById() {
        // Given
        ProductDto productDto = new ProductDto(1L, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012");
        when(productService.getProductById(1L)).thenReturn(productDto);

        // When
        ProductDto result = productController.getProductById(1L);

        // Then
        assertAll(
                () -> assertEquals("Product 1", result.getName()),
                () -> assertEquals(BigDecimal.valueOf(10.0), result.getPrice()),
                () -> assertEquals(5, result.getCount()),
                () -> assertEquals("0123456789012", result.getPhoneNumber())
        );
    }

    @Test
    void testCreateProduct() {
        // Given
        ProductDto productDto = new ProductDto(null, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012");
        ProductDto savedProductDto = new ProductDto(1L, "Product 1", BigDecimal.valueOf(10.0), 5, "0123456789012");
        when(productService.createProduct(productDto)).thenReturn(savedProductDto);

        // When
        ProductDto result = productController.createProduct(productDto);

        // Then
        assertEquals("Product 1", result.getName());
    }

    @Test
    void testUpdateProduct() {
        // Given
        ProductDto productDto = new ProductDto(null, "Updated Product", BigDecimal.valueOf(20.0), 10, "0123456789012");
        ProductDto updatedProductDto = new ProductDto(1L, "Updated Product", BigDecimal.valueOf(20.0), 10, "0123456789012");
        when(productService.updateProduct(1L, productDto)).thenReturn(updatedProductDto);

        // When
        ProductDto result = productController.updateProduct(1L, productDto);

        // Then
        assertEquals("Updated Product", result.getName());
    }

    @Test
    void testDeleteProductById() {
        // Given
        // When
        productController.deleteProductById(1L);

        // Then
        verify(productService).deleteProductById(1L);
    }

    @Test
    void testDeleteAllProducts() {
        // Given
        // When
        productController.deleteAllProducts();

        // Then
        verify(productService).deleteAllProduct();
    }
}
