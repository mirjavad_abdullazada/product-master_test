package az.atl.product.service;

import az.atl.product.model.dto.ProductDto;

import java.util.List;


public interface ProductService {
    List<ProductDto> getAllProduct();
    ProductDto getProductById(Long id);
    ProductDto createProduct(ProductDto productDto);
    ProductDto updateProduct(Long id,ProductDto productDto);
    void deleteProductById(Long id);
    void deleteAllProduct();


}
