package az.atl.product.service.impl;

import az.atl.product.dao.entity.ProductEntity;
import az.atl.product.dao.repository.ProductRepository;
import az.atl.product.exception.ProductNotFoundException;
import az.atl.product.model.dto.ProductDto;
import az.atl.product.service.ProductService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static az.atl.product.model.mapper.ProductMapper.buildDto;
import static az.atl.product.model.mapper.ProductMapper.buildDtoList;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);


    @Override
    public List<ProductDto> getAllProduct() {
        List<ProductEntity> productDtoList = productRepository.findAll();
        return buildDtoList(productDtoList);
    }

    @Override
    public ProductDto getProductById(Long id) {
        ProductEntity productEntity = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException("Product not found with id:" + id));
        return buildDto(productEntity);
    }

    @Override
    public ProductDto createProduct(ProductDto productDto) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName(productDto.getName());
        productEntity.setPrice(productDto.getPrice());
        productEntity.setCount(productDto.getCount());
        productEntity.setPhoneNumber(productDto.getPhoneNumber());
        ProductEntity savedProduct = productRepository.save(productEntity);
        return buildDto(savedProduct);
    }

    @Override
    public ProductDto updateProduct(Long id, ProductDto productDto) {
        ProductEntity productEntity = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException("Product not found with id:" + id));
        productEntity.setName(productDto.getName());
        productEntity.setPrice(productDto.getPrice());
        productEntity.setCount(productDto.getCount());
        productEntity.setPhoneNumber(productDto.getPhoneNumber());
        ProductEntity updatedProduct = productRepository.save(productEntity);
        return null;
    }

    @Override
    public void deleteProductById(Long id) {
        if (!productRepository.existsById(id)) {
            throw new ProductNotFoundException("Product not found with id: " + id);
        }
        productRepository.deleteById(id);
    }

    @Override
    public void deleteAllProduct() {
        log.info("Deleting all products");
        productRepository.deleteAll();
        log.info("All products deleted successfully");
    }


}

