package az.atl.product.util;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.experimental.UtilityClass;


@UtilityClass
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhoneNumberMaskUtil {
    static final String MASK_CHARACTER = "*";
    static final int VISIBLE_DIGITS_START_INDEX = 7;
    static final int VISIBLE_DIGITS_END_INDEX = 2;
    static final int VISIBLE_DIGITS_ALL = 13;

    public static String maskPhoneNumber(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.length() < VISIBLE_DIGITS_ALL) {
            throw new IllegalArgumentException("Invalid phone number");
        }

        String visibleDigits = phoneNumber.substring(VISIBLE_DIGITS_START_INDEX, phoneNumber.length() - VISIBLE_DIGITS_END_INDEX);
        String maskedDigits = MASK_CHARACTER.repeat(visibleDigits.length());

        return phoneNumber.substring(0, VISIBLE_DIGITS_START_INDEX) + maskedDigits + phoneNumber.substring(phoneNumber.length() - VISIBLE_DIGITS_END_INDEX);
    }
}

